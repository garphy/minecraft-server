FROM openjdk:buster as builder
ENV BUILDTOOLS_BUILD=130
RUN apt-get update && apt-get install git wget
RUN wget https://hub.spigotmc.org/jenkins/job/BuildTools/${BUILDTOOLS_BUILD}/artifact/target/BuildTools.jar
ENV VERSION=1.17
ENV BUILD=${VERSION}-${BUILDTOOLS_BUILD}
RUN java -jar BuildTools.jar --rev ${VERSION}

FROM openjdk:16
ENV VERSION=1.17
COPY --from=builder /spigot-${VERSION}.jar /app/spigot.jar
RUN mkdir /storage
WORKDIR /storage
VOLUME /storage
EXPOSE 25565
CMD ["java", "-Xms256m", "-Xmx1G", "-XX:+UseConcMarkSweepGC", "-jar", "/app/spigot.jar"]
